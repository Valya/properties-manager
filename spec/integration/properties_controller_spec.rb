require 'swagger_helper'

describe 'Properties API' do
  let(:properties){  create_list :myproperty, 5 }
  let(:property) {{ property: attributes_for(:myproperty) }}
  let(:id){ properties[0].id }

  path '/api/v1/properties' do

    # let(:q) {}
    get 'List of properties' do 
      tags 'Properties'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :q, in: :query, type: :string, required: false

      response '200', 'success' do
        schema type: :array,
          items: {
            properties: {
              id: { type: :integer },
              name: { type: :string },
              description: { type: :string }
            }
          }
      
        run_test!
      end
    end

    post 'Creates a property' do
      tags 'Properties'
      consumes 'application/json'
      parameter name: :property, in: :body, schema: { type: :object,
        properties: {
          property: {
            type: :object,
            properties: {
              name: { type: :string },
              description: { type: :string },
              address: { type: :string }, 
              unit_attributes: {
                type: :array,
                items: {
                  properties:{
                    number: { type: :number},
                    area: { type: :number}
                  }
                }
              }
            }
          }
          
        }
      }
      
      response '201', 'property created' do
        run_test!
      end
    end
  end

  path '/api/v1/properties/{id}' do

    get 'Retrieves a property' do
      tags 'Properties'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :string

      response '200', 'property found' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string },
            description: { type: :text },
            address: { type: :string }
          }
        run_test!
      end

      response '404', 'property not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end


    put 'Update property' do
      tags 'Properties'
      consumes 'application/json'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :string
      parameter name: :property, in: :body, schema: { type: :object,
        properties: {
          property: {
            type: :object,
            properties: {
              name: { type: :string },
              description: { type: :string },
              address: { type: :string }, 
              unit_attributes: {
                type: :array,
                items: {
                  properties:{
                    number: { type: :number},
                    area: { type: :number}
                  }
                }
              }
            }
          }
          
        }
      }
      response '200', 'property found' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string },
            description: { type: :text },
            address: { type: :string }
          }

        run_test!
      end

    end

    delete 'Delete property' do
      tags 'Properties'
      parameter name: :id, :in => :path, :type => :string
      response '200', 'property found' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string },
            description: { type: :text },
            address: { type: :string }
          }

        run_test!
      end
    end

   
  end

  path '/api/v1/properties/{id}/archive' do

    get 'Archive property' do 
      tags 'Properties'
      parameter name: :id, :in => :path, :type => :string

      response '200', 'property found' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            name: { type: :string },
            description: { type: :text },
            address: { type: :string }
          }
        run_test!
      end

    end
  end
end
