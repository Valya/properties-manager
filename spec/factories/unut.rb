FactoryBot.define do
  factory :unit do 
    number { 2 }
    area { 20.3 }
  end
end