FactoryBot.define do
  factory :myproperty, class: 'Property' do 
    name { "pinkunicorn" }
    description { "Pink" }
    address { "Unicorn" }

    after :create do |property|
      create_list :unit, 5, property: property
    end
  end
end
