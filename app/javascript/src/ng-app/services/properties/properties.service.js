(function() {
  'use strict';

  angular
    .module('service:propertiesService', [])
    .factory('Properties', ['$http', function( $http) {
      var Properties = {};

      Properties.list = function() {
        return $http.get('/api/v1/properties').then(function(res) {
          return res.data;
        });
      };     
      
      Properties.create = function(body) {
        return $http.post('/api/v1/properties', body).then(function(res) {
          return res.data;
        });
      };

      Properties.get = function(propertyId) {
        return $http.get('/api/v1/properties/' + propertyId).then(function(res) {
          return res.data;
        });
      }

      Properties.update = function(property) {
        return $http.put('/api/v1/properties/' + property.id, property).then(function(res) {
          return res.data;
        });
      }

      Properties.archive = function(propertyId) {
        return $http.get('/api/v1/properties/' + propertyId + '/archive').then(function(res) {
          return res.data;
        });
      }

      Properties.search = function(q) {
        return $http.get('/api/v1/properties?q='+q).then(function(res) {
          return res.data;
        });
      }

      return Properties;
    }]);
})();
