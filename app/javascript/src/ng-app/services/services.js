(function() {
  'use strict';

  require('./properties/properties.service')

  angular
    .module('propertiesManager:services', [
      'service:propertiesService',
    ]);
})();
