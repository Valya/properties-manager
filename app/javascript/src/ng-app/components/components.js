(function() {
  'use strict';

  require('./property/property.controller')
  require('./property/property-edit.controller')
  require('./property/property-new.controller')
  // require('./dashboard/dashboard.controller')
  angular
    .module('propertiesManager:componentControllers', [
      'componentControllers:propertyController',
      'componentControllers:propertyNewController',
      'componentControllers:propertyEditController',
      // 'componentControllers:dashboardController'
    ]);
})();