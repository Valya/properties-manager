(function() {
  'use strict';

  angular
    .module('componentControllers:dashboardController', [])
    .controller('DashboardController', ['$scope', '$state', 'Properties', function($scope, $state, Properties) {
      var pc = this;

    }])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('dashboard', {
        url: '/',
        templateUrl: 'dashboard.html',
        controller: 'DashboardController as dc'
        // resolve: {
        //   properties: [function() {
        //     return [];
        //   }]
        // }
      })
      //  $urlRouterProvider.when('/', '/dashboard');
    }])
})();
