(function() {
  'use strict';

  angular
    .module('componentControllers:propertyNewController', [])
    .controller('PropertyNewController', ['$scope', '$state', 'Properties', function($scope, $state, Properties) {
      var vm = this;

      $scope.property = {
        name: '',
        description: '',
        address: '',
        units_attributes: [{
          number: null,
          area: null
        }]
      }

      $scope.saveProperty = function(property){
        Properties.create(property).then(function(res){
          $state.go('properties')
        })
      }
      
    }])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('properties-new', {
        url: '/properties/new',
        templateUrl: 'properties-new.html',
        controller: 'PropertyNewController as pnc',
        resolve: {
         
        }
      })
    }])
})();


