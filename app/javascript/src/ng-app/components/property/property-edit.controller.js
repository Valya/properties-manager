(function() {
  'use strict';

  angular
    .module('componentControllers:propertyEditController', [])
    .controller('PropertyEditController', ['$scope', '$state', 'property', 'Properties',  function($scope, $state, property, Properties) {
      var vm = this;
      $scope.property = property
      $scope.saveProperty = function(property){
        Properties.update(property).then(function(res){
          $state.go('properties')
        })
      }
    }])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('properties-edit', {
        url: '/properties/:propertyId/edit',
        templateUrl: 'properties-edit.html',
        controller: 'PropertyEditController as pec',
        resolve: {
          property: ['$stateParams','Properties', function($stateParams, Properties) {
            return Properties.get($stateParams.propertyId).then(function(property){
              property.units_attributes = property.units;
              return property;
            })
          }]
        }
      })
    }])
})();