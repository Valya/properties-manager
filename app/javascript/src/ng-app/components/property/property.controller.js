(function() {
  'use strict';

  angular
    .module('componentControllers:propertyController', [])
    .controller('PropertyController', ['$scope', '$state', 'properties', 'Properties', '$uibModal', function($scope, $state, properties, Properties, $uibModal) {
      var pc = this;
      pc.foundProperties = properties;

      $scope.archiveProperty = function(data){
        data.event.stopPropagation();
        return Properties.archive( data.propertyId ).then(function(res){
          $state.reload();
        })
      }
      $scope.editUnit = function(unitId){
        $uibModal.open({
          templateUrl: 'property-unit-edit-modal.html',
          size: 'sm',
          controller: function($scope) {
            $scope.name = 'top';  
          }
        });
      }


      $scope.q = {}

      $scope.searchProperties = function(object) {
        // debugger
        return Properties.search( JSON.stringify(object) ).then(function(res){
          pc.foundProperties = res;
          // $state.reload();
        })
      }
    }])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('properties', {
        url: '/properties',
        templateUrl: 'properties-list.html',
        controller: 'PropertyController as $ctrl',
        resolve: {
          properties: ['Properties', function(Properties) {
            return  Properties.list();
          }]
        }
      })
      $urlRouterProvider.when('/', '/properties');
    }])
})();
