(function() {
  'use strict';

  angular
    .module('directive:propertyForm', [])
    .directive('propertyForm', function() {
      return {
        scope: {
          property: '=',
          save: '&'
        },
        templateUrl: 'property-form.html',
        link: function(scope, ele, attr){
          scope.addUnit = function(){
            scope.property.units_attributes.push({
              number: '',
              area: ''
            })
          }

          scope.deleteUnit = function(index){
            scope.property.units_attributes.splice(index, 1)
          }
        }
      };
    })
})();
