(function() {
  'use strict';

  angular
    .module('directive:searchForm', [])
    .directive('searchForm', function() {
      return {
        scope: {
          object: '=',
          search: '&'
        },
        templateUrl: 'search-form.html'
      };
    })
})();
