(function() {
  'use strict';

  require('./property/property-form')
  require('./property/search-form')
  
  angular
    .module('propertiesManager:directives', [
      'directive:propertyForm',
      'directive:searchForm'
    ]);
})();