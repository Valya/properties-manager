(function() {

  'use strict';

  angular
    .module('propertiesManager', [
      'ui.router',
      'ui.bootstrap',
      'restangular',
      'propertiesManager:componentControllers',
      'propertiesManager:services',
      'propertiesManager:directives'
    ])
    .config(['$locationProvider', 'RestangularProvider', '$urlRouterProvider', function( $locationProvider, RestangularProvider, $urlRouterProvider ) {

      RestangularProvider.setBaseUrl('/api/v1');
      // $urlRouterProvider.otherwise('properties');
      // enable HTML5 mode for SEO
      $locationProvider.html5Mode(true);
    }])

})();