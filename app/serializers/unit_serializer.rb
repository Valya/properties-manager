class UnitSerializer < ActiveModel::Serializer
  attributes :id, :number, :area

  def area
    object.area.to_f
  end
end