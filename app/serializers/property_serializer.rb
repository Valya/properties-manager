class PropertySerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :address, :status
  has_many :units
end