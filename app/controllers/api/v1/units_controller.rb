class Api::V1::UnitsController < ApiController
  
  def update
    @unit = unit
    if @unit.update(unit_params)
      render json: @property, status: 200
    else
      render json: {error: @property.errors }, status: 400
    end
  end

  private 

  def unit_params
    params.require(:unit).permit(:id, :number, :area)
  end

  def unit
    Unit.find(params[:id])
  end
end
