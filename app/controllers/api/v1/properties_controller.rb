class Api::V1::PropertiesController < ApiController
  def index
    # @properties = Property.all
    # render json: @properties

    if params[:q]
      @q = Property.ransack(JSON.parse(params[:q]))
      @properties = @q.result.includes(:units)
    else
      @properties = Property.all
    end
    render json: @properties
  end

  def show
    begin
      @property = Property.find(params[:id])
      render json: @property
    rescue => exception
      render json: {error: 'not_found' }, status: 404
    end

  end

  def create
    @property = Property.new(property_params)
    if @property.save
      render json: @property, status: 201
    else
      render json: {error: @property.errors }, status: 400
    end
  end

  def update
    @property = Property.find(params[:id])
    Property.transaction do
      @property.units.where.not(id: (property_params[:units_attributes] || []).map{|el| el[:id]}).destroy_all
      @property.update(property_params)
    end
    render json: @property, status: 200
  rescue ActiveRecord::RecordInvalid
    render json: {error: @property.errors }, status: 400
  end

  def destroy
    @property = property
    if @property.destroy
      render json: @property, status: 200
    else
      render json: {error: @property.errors }, status: 400
    end
  end

  def archive
    @property = property
    if @property.achive
      render json: @property, status: 200
    else
      render json: {error: @property.errors }, status: 400
    end
  end

  private 

  def property_params
    params.require(:property).permit(
      :id, :name, :description, :address,
      units_attributes: [:id, :number, :area]
    )
  end

  def property
    Property.find(params[:id])
  end
end
