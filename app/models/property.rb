class Property < ApplicationRecord
  has_many :units, dependent: :destroy
  enum status: [:active, :archived]
  default_scope { where.not(:status => Property.statuses[:archived])}

  accepts_nested_attributes_for :units, allow_destroy: true

  def achive
    update(status: Property.statuses[:archived])
  end
end
