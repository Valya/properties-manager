
## Setup 
To run the application execute the following commands:
```
bundle
yarn install
rails db:setup
rails db:migrate
rails db:seed
rails s
```
## Deployed application 
To see how it looks follow [the link](https://properties-manager.herokuapp.com/)

## Application information 
This application contains frontend written on AngularJS which comunicates with backend API on Ruby
Ruby version is 2.6.3
AngularJS version 1.7.9

Swagger documentation on backend API [is here](https://properties-manager.herokuapp.com/api-docs/index.html)

