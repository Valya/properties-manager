Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  namespace 'api' do
    namespace 'v1' do
      resources :properties do 
        member do
          get 'archive'
        end
        resources :units, only: [:update]
      end
    end
  end

  get "*path" => "application#index"
  root 'application#index'
end
