class CreateUnits < ActiveRecord::Migration[6.0]
  def change
    create_table :units do |t|
      t.integer :number
      t.decimal :area
      t.references :property, index: true

      t.timestamps
    end
  end
end
