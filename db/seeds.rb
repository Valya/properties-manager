# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Property.create(name:'House on Serdicha str', description: 'House on Serdicha str', address: 'Serdicha str. 9, Minsk', units_attributes: [{number: 1, area: 23.4}, {number: 2, area: 45}])
Property.create(name:'House on Zudro str', description: 'House on Zudro str', address: 'Zudro str. 17, Minsk', units_attributes: [{number: 1, area: 45}, {number: 2, area: 55}])
Property.create(name:'House on Belskogo str', description: 'House on Belskogo str', address: 'Belskogo str. 17, Minsk', units_attributes: [{number: 1, area: 45}, {number: 2, area: 55}])